# -*- coding: utf-8 -*-

# Run the build process by running the command 'python setup.py build'
#
# If everything works well you should find a subdirectory in the build
# subdirectory that contains the files needed to run the script without Python

from cx_Freeze import setup, Executable

build_exe_options = {
    'create_shared_zip': False,
    'include_msvcr': True  # skip error msvcr100.dll missing
}

executables = [
    Executable('SystemInformation.py',
               appendScriptToExe=True)
]

setup(name='SysInfo',
      version='0.1',
      description='Windows system information gatherer',
      options={'build_exe': build_exe_options},
      executables=executables
      )
