Usage
-----
- Double click on application file.
- or in a Cmd shell:

        \> .\SystemInformation.exe

Output are both, a file named ``system_information.txt`` and stdout. Both
display the same information: CPU, Video devices, RAM, Disk free space and OS
version.

Requires
--------
**Running from source or building:** Python 3.x

**Running binary:** nothing is required.

Building
--------
    \> pip install py2exe
    \> python -m py2exe.build_exe -b1 SystemInformation.py

Download
--------
- [Release 0.2](http://grulic.org.ar/~dalonso/SysInfo-0.2.zip)
- [Release 0.1](http://grulic.org.ar/~dalonso/SysInfo-0.1.zip)
